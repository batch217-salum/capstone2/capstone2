const Product = require("../models/Product.js");
const User = require("../models/User.js");

module.exports.addProduct = (data) => {
	return User.findById(data.user.id).then(result => {
		if(result == null){
			return false
		}
		else{
			if(result.isAdmin == false){
				return false
			}
			else{
				return Product.findOne({name : data.content.name}).then(result => {
					if(result != null){
						return false
					}
					else{
						let newProduct = new Product({
							name : data.content.name,
							description : data.content.description,
							price : data.content.price
						})

						return newProduct.save().then((user, error) => {
							if(error){
								return false
							}
							else{
								return true
							}
						})
					}
				})
			}
		}
	})
}

module.exports.getAllProducts = () => {
	return Product.find().then(result => {
		if(result == null){
			return false
		}
		else{
			return result
		}
	})
}

module.exports.getProducts = () => {
	return Product.find({isActive : true}).then(result => {
		if(result == null){
			return false
		}
		else{
			return result
		}
	})
}

module.exports.getSpecificProduct = (productId) => {
	return Product.findById(productId).then(result => {
		if(result == null){
			return false
		}
		else{
			return result;
		}
	})
}

module.exports.archiveProduct = (data) => {
	return Product.findById(data.productId).then(result => {
		if(result == null){
			return false
		}
		else{
			return User.findById(data.user.id).then(result => {
				if(result == null){
					return false
				}
				else if(result.isAdmin == false && result != null){
					return false
				}
				else{
					return Product.findByIdAndUpdate(data.productId, {isActive : false})
					.then((archivedProduct, error) => {
						if(error){
							return false
						}
						else{
							return true
						}
					})
				}
			})
		}
	})
}

module.exports.unarchiveProduct = (data) => {
	return Product.findById(data.productId).then(result => {
		if(result == null){
			return false
		}
		else{
			return User.findById(data.user.id).then(result => {
				if(result == null){
					return false
				}
				else if(result.isAdmin == false && result != null){
					return false
				}
				else{
					return Product.findByIdAndUpdate(data.productId, {isActive : true})
					.then((archivedProduct, error) => {
						if(error){
							return false
						}
						else{
							return true
						}
					})
				}
			})
		}
	})
}


module.exports.updateProduct = (data) => {
	return Product.findById(data.content.productId).then(result => {
		if(result == null){
			return false
		}
		else{
			return User.findById(data.user.id).then(result => {
				if(result.isAdmin == false){
					return false
				}
				else{
					return Product.findByIdAndUpdate(data.content.productId, {
						name : data.content.name,
						description : data.content.description,
						price : data.content.price
					})
					.then((updatedProduct, error) => {
						if(error){
							return false
						}
						else{
							return true
						}
					})
				}
			})
		}
	})
}